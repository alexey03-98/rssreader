package com.nelyubin.rssreader.model;

import android.support.annotation.NonNull;

import java.util.Comparator;
import java.util.Date;
import lombok.Data;

@Data

public class Item implements Comparable<Item>
{
    private String titleOfFeed;
    private String Title;
    private String UrlLink;
    private String Description;
    private String linkOfIconFeed;
    private Date PublishedDate;

    @Override
    public int compareTo(@NonNull final Item item)
    {
        return this.PublishedDate.compareTo(item.PublishedDate);
    }
    public static Comparator<Item> DateComparator = new Comparator<Item>() {

        @Override
        public int compare(Item item1, Item item2) {
            return item2.PublishedDate.compareTo(item1.PublishedDate);
        }
    };
}
