package com.nelyubin.rssreader.model;

import lombok.Data;

@Data

public final class Feed
{
    private String Title;
    private String Link;
    private String LinkOfRssOrAtomFeed;
    private String LinkOfIconOFFeed;
    private boolean isRssFeed;
}
