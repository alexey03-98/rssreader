package com.nelyubin.rssreader.listeners;

import android.view.View;
import android.widget.EditText;

import com.nelyubin.rssreader.service.BackgroundAddingChannel;

public final class AddChannelAndStartDownloadButtonListener implements View.OnClickListener
{

    private EditText link;

    public AddChannelAndStartDownloadButtonListener(EditText link)
    {
        this.link = link;
    }

    @Override
    public final void onClick(final View view)
    {
        view.getContext().startService(BackgroundAddingChannel.startServiceForAddingChannel(view.getContext(),link.getText().toString(),false));
    }
}
