package com.nelyubin.rssreader.parsers;

import android.content.ContentValues;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import com.nelyubin.rssreader.model.Item;
import com.nelyubin.rssreader.database.SqlDB;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Locale;

public final class RssParser implements Parser
{
    @Override
    public void parseAndSaveOnDatabase(SQLiteDatabase database, XmlPullParser xmlPullParser, String titleOfFeed, String linkOfIconFeed)
            throws IOException, XmlPullParserException
    {
        ContentValues contentValuesForItem = new ContentValues();

        Item item = new Item();

        boolean insideItem = false;

        int eventType;

        DateFormat df = DateFormat.getDateInstance(DateFormat.LONG,new Locale("ru","RU"));

        try {

            eventType = xmlPullParser.getEventType();

            while (eventType != XmlPullParser.END_DOCUMENT)
            {
                if (eventType == XmlPullParser.START_TAG)
                {
                    if (xmlPullParser.getName().equalsIgnoreCase("item"))
                    {
                        insideItem = true;
                    }
                    else if (xmlPullParser.getName().equalsIgnoreCase("title"))
                    {
                        if (insideItem)
                        {
                            item.setTitle(xmlPullParser.nextText());
                        }
                    }
                    else if (xmlPullParser.getName().equalsIgnoreCase("link"))
                    {
                        if (insideItem)
                        {
                            item.setUrlLink(xmlPullParser.nextText());
                        }
                    }
                    else if (xmlPullParser.getName().equalsIgnoreCase("description"))
                    {
                        if (insideItem)
                        {
                            item.setDescription(xmlPullParser.nextText());
                        }
                    }
                }

                else if (eventType == XmlPullParser.END_TAG && xmlPullParser.getName().equalsIgnoreCase("item"))
                {
                    insideItem = false;
                    contentValuesForItem.put(SqlDB.KEY_FEED, titleOfFeed);
                    contentValuesForItem.put(SqlDB.KEY_TITLE, item.getTitle());
                    contentValuesForItem.put(SqlDB.KEY_LINK, item.getUrlLink());
                    contentValuesForItem.put(SqlDB.KEY_DESCRIPTION, item.getDescription());
                    contentValuesForItem.put(SqlDB.KEY_DATE, df.format(new Date()));
                    contentValuesForItem.put(SqlDB.KEY_LINK_OF_ICON_FEED,linkOfIconFeed);
                    if(!SqlDB.suchAnItemAlreadyAdded(database,item.getTitle(), item.getUrlLink()))
                    {
                        database.insert(SqlDB.TABLE_ALL_ITEMS, null, contentValuesForItem);
                    }
                }
                eventType = xmlPullParser.next();
            }
        }
        catch (SQLException e){e.printStackTrace();}
    }
}
