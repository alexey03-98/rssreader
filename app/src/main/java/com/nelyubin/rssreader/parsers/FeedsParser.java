package com.nelyubin.rssreader.parsers;

import com.nelyubin.rssreader.model.Feed;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

public final class FeedsParser
{
    public static Feed parseFieldsFeed(XmlPullParser xmlPullParser) throws XmlPullParserException,IOException
    {
        Feed feed = new Feed();
        int eventType;
        boolean isImageTag = false;
        boolean gotAllFields = false;
        eventType = xmlPullParser.getEventType();
        while (!gotAllFields)
        {
            if (eventType == XmlPullParser.START_TAG)
            {
                if(xmlPullParser.getName().equalsIgnoreCase("item"))
                {
                    gotAllFields=true;
                }
                else if (xmlPullParser.getName().equalsIgnoreCase("channel") && !isImageTag)
                {
                    feed.setRssFeed(true);
                }
                else if (xmlPullParser.getName().equalsIgnoreCase("feed") && !isImageTag)
                {
                    feed.setRssFeed(false);
                }
                else if (xmlPullParser.getName().equalsIgnoreCase("image") && !isImageTag)
                {
                    isImageTag = true;
                }
                else if (xmlPullParser.getName().equalsIgnoreCase("url") && isImageTag)
                {
                    feed.setLinkOfIconOFFeed(xmlPullParser.nextText());
                    isImageTag = false;
                }
                else if (xmlPullParser.getName().equalsIgnoreCase("title") && !isImageTag)
                {
                    feed.setTitle(xmlPullParser.nextText());
                }
                else if (xmlPullParser.getName().equalsIgnoreCase("link") && !isImageTag)
                {
                    if (feed.isRssFeed())
                    {
                        feed.setLink(xmlPullParser.nextText());
                    }
                    else
                    {
                        for (int i = 0; i < xmlPullParser.getAttributeCount(); i++)
                        {
                            if ("href".equals(xmlPullParser.getAttributeName(i)))
                            {
                                feed.setLink(xmlPullParser.getAttributeValue(i));
                            }
                        }
                    }
                }
            }
            if(!gotAllFields) {
                eventType = xmlPullParser.next();
            }
        }
        return feed;
    }
}
