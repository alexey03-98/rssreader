package com.nelyubin.rssreader.parsers;

import android.content.ContentValues;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import com.nelyubin.rssreader.model.Item;
import com.nelyubin.rssreader.database.SqlDB;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;

public final class AtomParser implements Parser
{

    @Override
    public final void parseAndSaveOnDatabase(final SQLiteDatabase database, final XmlPullParser xmlPullParser,final String titleOfFeed, final String linkOfIconFeed)
            throws IOException, XmlPullParserException
    {
        ContentValues contentValuesForItem = new ContentValues();
        Item item = new Item();
        boolean insideItem = false;
        int eventType = xmlPullParser.getEventType();
        try {
            while (eventType != XmlPullParser.END_DOCUMENT)
            {
                if (eventType == XmlPullParser.START_TAG)
                {
                    if (xmlPullParser.getName().equalsIgnoreCase("entry"))
                    {
                        insideItem = true;
                    }
                    else if (xmlPullParser.getName().equalsIgnoreCase("title"))
                    {
                        if (insideItem) {
                            item.setTitle(xmlPullParser.nextText());
                        }
                    }
                    else if (xmlPullParser.getName().equalsIgnoreCase("link"))
                    {
                        if (insideItem)
                        {
                            for (int i = 0; i < xmlPullParser.getAttributeCount(); i++)
                            {
                                if ("href".equals(xmlPullParser.getAttributeName(i)))
                                {
                                    item.setUrlLink(xmlPullParser.getAttributeValue(i));
                                }
                            }
                        }
                    }
                    else if (xmlPullParser.getName().equalsIgnoreCase("summary"))
                    {
                        if (insideItem)
                        {
                            item.setDescription(xmlPullParser.nextText());
                        }
                    }
                    else if (xmlPullParser.getName().equalsIgnoreCase("updated"))
                    {
                        if (insideItem)
                        {
                            try
                            {
                                item.setPublishedDate(DateFormat.getInstance().parse(xmlPullParser.nextText()));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
                else if (eventType == XmlPullParser.END_TAG && xmlPullParser.getName().equalsIgnoreCase("entry"))
                {
                    insideItem = false;
                    contentValuesForItem.put(SqlDB.KEY_FEED,titleOfFeed);
                    contentValuesForItem.put(SqlDB.KEY_TITLE, item.getTitle());
                    contentValuesForItem.put(SqlDB.KEY_LINK, item.getUrlLink());
                    contentValuesForItem.put(SqlDB.KEY_DESCRIPTION, item.getDescription());
                    contentValuesForItem.put(SqlDB.KEY_DATE, item.getPublishedDate().toString());
                    contentValuesForItem.put(SqlDB.KEY_LINK_OF_ICON_FEED, linkOfIconFeed);
                    if(!SqlDB.suchAnItemAlreadyAdded(database,item.getTitle(), item.getUrlLink()))
                    {
                        database.insert(SqlDB.TABLE_ALL_ITEMS, null, contentValuesForItem);
                    }
                }
                eventType = xmlPullParser.next();
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }
}
