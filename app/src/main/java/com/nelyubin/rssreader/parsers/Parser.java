package com.nelyubin.rssreader.parsers;

import android.database.sqlite.SQLiteDatabase;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import java.io.IOException;

public interface Parser
{
    void parseAndSaveOnDatabase(SQLiteDatabase database, XmlPullParser xmlPullParser, String titleOfFeed, String linkOfIconFeed)
            throws IOException, XmlPullParserException;
}
