package com.nelyubin.rssreader.network;

import android.app.Service;
import android.content.ContentValues;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.webkit.URLUtil;
import com.nelyubin.rssreader.databus.DownloadIsFinishedReceiver;
import com.nelyubin.rssreader.databus.WrongLinkOfFeedReceiver;
import com.nelyubin.rssreader.model.Feed;
import com.nelyubin.rssreader.parsers.AtomParser;
import com.nelyubin.rssreader.parsers.FeedsParser;
import com.nelyubin.rssreader.parsers.Parser;
import com.nelyubin.rssreader.parsers.RssParser;
import com.nelyubin.rssreader.database.SqlDB;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import javax.net.ssl.HttpsURLConnection;

public class ThreadForAddingAndRefreshingFeeds implements Runnable
{
    private Service service;

    private SqlDB sqlDB;

    private String linkOfFeed;

    private boolean isRefreshFeeds;

    private HttpsURLConnection httpsURLConnection;

    private InputStream inputStream;

    public ThreadForAddingAndRefreshingFeeds(Service service, String linkOfFeed,SqlDB sqlDB, boolean isRefreshFeeds)
    {
        this.service = service;
        this.linkOfFeed = linkOfFeed;
        this.sqlDB=sqlDB;
        this.isRefreshFeeds = isRefreshFeeds;
    }

    @Override
    public void run()
    {
        final SQLiteDatabase database = sqlDB.getWritableDatabase();
        Parser parser;
        ContentValues contentValuesForFeed = new ContentValues();
        Feed feed;

        try
        {
            if(!isRefreshFeeds)
            {
                if (!URLUtil.isHttpsUrl(linkOfFeed) && !URLUtil.isHttpUrl(linkOfFeed))
                    linkOfFeed = "https://" + linkOfFeed;

                final URL url = new URL(linkOfFeed);
                final XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
                httpsURLConnection = (HttpsURLConnection) url.openConnection();
                inputStream = httpsURLConnection.getInputStream();
                factory.setNamespaceAware(false);
                final XmlPullParser xmlPullParser = factory.newPullParser();
                xmlPullParser.setInput(inputStream, "UTF_8");
                feed = FeedsParser.parseFieldsFeed(xmlPullParser);
                if(null != feed.getTitle() && null != feed.getLink())
                {
                    if (feed.isRssFeed()) {
                        parser = new RssParser();
                    } else {
                        parser = new AtomParser();
                    }
                    parser.parseAndSaveOnDatabase(database, xmlPullParser, feed.getTitle(), feed.getLinkOfIconOFFeed());
                    contentValuesForFeed.put(SqlDB.KEY_LINK_FEED, feed.getLink());
                    contentValuesForFeed.put(SqlDB.KEY_LINK_OF_RSS_OR_ATOM_FEED, linkOfFeed);
                    contentValuesForFeed.put(SqlDB.KEY_TITLE_FEED, feed.getTitle());
                    contentValuesForFeed.put(SqlDB.KEY_LINK_OF_ICON_FEED, feed.getLinkOfIconOFFeed());

                    if (feed.isRssFeed())
                        contentValuesForFeed.put(SqlDB.KEY_IS_RSS_FEED, 1);
                    else
                        contentValuesForFeed.put(SqlDB.KEY_IS_RSS_FEED, 0);

                    database.insert(SqlDB.TABLE_ALL_FEEDS, null, contentValuesForFeed);
                }
            }
            else
            {
                final ArrayList<Feed> feedArrayList = SqlDB.getAllFeedsFromDatabase(service);

                for (int i = 0; i < feedArrayList.size(); i++)
                {
                    final URL url = new URL(feedArrayList.get(i).getLinkOfRssOrAtomFeed());
                    final XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
                    httpsURLConnection = (HttpsURLConnection) url.openConnection();
                    inputStream = httpsURLConnection.getInputStream();
                    factory.setNamespaceAware(false);
                    final XmlPullParser xmlPullParser = factory.newPullParser();
                    xmlPullParser.setInput(inputStream, "UTF_8");
                    if(feedArrayList.get(i).isRssFeed())
                        parser = new RssParser();
                    else
                        parser = new AtomParser();
                    parser.parseAndSaveOnDatabase(database,xmlPullParser,feedArrayList.get(i).getTitle(),feedArrayList.get(i).getLinkOfIconOFFeed());
                }
            }
            final Intent intent = new Intent(DownloadIsFinishedReceiver.DOWNLOAD_IS_FINISHED);
            intent.putExtra(DownloadIsFinishedReceiver.DOWNLOAD_IS_FINISHED, true);
            service.sendBroadcast(intent);
        }
        catch (final XmlPullParserException e)
        {
            e.printStackTrace();
        }
        catch(final IOException e)
        {
            Intent intent = new Intent(WrongLinkOfFeedReceiver.WRONG_LINK_OF_FEED);
            service.sendBroadcast(intent);
            service.stopSelf();
        }
        catch(final SQLException e)
        {
            sqlDB.close();
        }
        finally
        {
            try
            {
                if(null != inputStream)
                    inputStream.close();
                if(null != httpsURLConnection)
                    httpsURLConnection.disconnect();
                if(null != sqlDB)
                    sqlDB.close();
            }
            catch (final IOException e)
            {
                service.stopSelf();
            }
        }
    }
}
