package com.nelyubin.rssreader.settings;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import com.nelyubin.rssreader.databus.StartUpServiceReceiver;

public final class AutomaticOperations
{
    private static final long ONE_DAY_ON_MILISECONDS = 86400000;

    public static void restartAlarmManagerForUpdate(final Context context, final long intervalBetweenStartOfService)
    {
        final AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        final Intent intent = new Intent(context, StartUpServiceReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0,
                intent, PendingIntent.FLAG_CANCEL_CURRENT );
        alarmManager.cancel(pendingIntent);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,intervalBetweenStartOfService*ONE_DAY_ON_MILISECONDS, intervalBetweenStartOfService*ONE_DAY_ON_MILISECONDS, pendingIntent);
    }

    public static void cancelAlarmManagerForUpdate(final Context context)
    {
        final AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        final Intent intent = new Intent(context, StartUpServiceReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0,
                intent, PendingIntent.FLAG_CANCEL_CURRENT );
        alarmManager.cancel(pendingIntent);
    }

    /*public static void restartAlarmManagerForDeletionReadNews(final Context context)
    {
        RssReaderPreferences rssReaderPreferences = new RssReaderPreferences();
        rssReaderPreferences.getRssReaderPreferences(context);
        long intervalBetweenStartOfService = rssReaderPreferences.getUpdatePeriod();
        final AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        final Intent intent = new Intent(context, StartUpServiceReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0,
                intent, PendingIntent.FLAG_CANCEL_CURRENT );
        alarmManager.cancel(pendingIntent);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,intervalBetweenStartOfService, intervalBetweenStartOfService, pendingIntent);
    }

    public final void cancelAlarmManagerForUpdate(final Context context)
    {
        final AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        final Intent intent = new Intent(context, StartUpServiceReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0,
                intent, PendingIntent.FLAG_CANCEL_CURRENT );
        alarmManager.cancel(pendingIntent);
    }*/
}
