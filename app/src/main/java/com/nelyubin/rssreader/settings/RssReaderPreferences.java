package com.nelyubin.rssreader.settings;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.nelyubin.rssreader.R;

import lombok.Getter;

@Getter

public final class RssReaderPreferences
{
    private boolean settingsIsChanged;

    private boolean automaticUpdate;

    private boolean useOnlyWiFi;

    private int updatePeriod;

    private int deletionReadPeriod;

    private int deletionUnreadPeriod;

    public void getRssReaderPreferences(Context context)
    {
        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(context);
        automaticUpdate = prefs.getBoolean(context.getString(R.string.PREFERENCE_KEY_AUTOMATIC_UPDATE), true);
        useOnlyWiFi = prefs.getBoolean(context.getString(R.string.PREFERENCE_KEY_USE_ONLY_WIFI), false);
        updatePeriod = Integer.parseInt(prefs.getString(context.getString(R.string.PREFERENCE_KEY_UPDATE_PERIOD),"1"));
        deletionReadPeriod = Integer.parseInt(prefs.getString(context.getString(R.string.PREFERENCE_KEY_AUTOMATIC_DELETION_READ),"1"));
        deletionUnreadPeriod = Integer.parseInt(prefs.getString(context.getString(R.string.PREFERENCE_KEY_AUTOMATIC_DELETION_UNREAD),"1"));
        prefs = context.getSharedPreferences(context.getString(R.string.PREFERENCE_KEY_SETTINGS_IS_CHANGED),Activity.MODE_PRIVATE);
        settingsIsChanged = prefs.getBoolean(context.getString(R.string.PREFERENCE_KEY_SETTINGS_IS_CHANGED),true);
    }

    public static void setSettingsIsChanged(final Context context, final boolean settingsIsChanged)
    {
        SharedPreferences prefs = context.getSharedPreferences(context.getString(R.string.PREFERENCE_KEY_SETTINGS_IS_CHANGED),Activity.MODE_PRIVATE);
        prefs.edit().putBoolean(context.getString(R.string.PREFERENCE_KEY_SETTINGS_IS_CHANGED),settingsIsChanged).apply();
    }
}
