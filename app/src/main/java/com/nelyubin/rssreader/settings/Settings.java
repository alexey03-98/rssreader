package com.nelyubin.rssreader.settings;

import android.content.Context;

public final class Settings
{
    public static void setSettings(Context context)
    {
        RssReaderPreferences rssReaderPreferences = new RssReaderPreferences();
        rssReaderPreferences.getRssReaderPreferences(context);
        if(rssReaderPreferences.isSettingsIsChanged())
        {
            if(rssReaderPreferences.isAutomaticUpdate())
            {
                AutomaticOperations.restartAlarmManagerForUpdate(context,rssReaderPreferences.getUpdatePeriod());
            }
            else
            {
                AutomaticOperations.cancelAlarmManagerForUpdate(context);
            }
            rssReaderPreferences.setSettingsIsChanged(context,false);
        }
    }
}
