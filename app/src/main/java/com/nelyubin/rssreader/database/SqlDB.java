package com.nelyubin.rssreader.database;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.nelyubin.rssreader.model.Feed;
import com.nelyubin.rssreader.model.Item;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Locale;

import lombok.val;

public final class SqlDB extends SQLiteOpenHelper
{
    private static final int DATABASE_VERSION = 1;

    private static final String DATABASE_NAME = "allItemsAndFeeds";

    public static final String TABLE_ALL_FEEDS ="allFeeds";

    public static final String TABLE_ALL_ITEMS = "allItems";

    private static final String TABLE_ALL_SAVED_ITEMS = "allSavedItems";

    private static final String KEY_ID = "_id";

    public static final String KEY_FEED = "feed";

    public static final String KEY_LINK_FEED = "linkFeed";

    public static final String KEY_LINK_OF_RSS_OR_ATOM_FEED = "linkOfRssOrAtomFeed";

    public static final String KEY_TITLE_FEED = "titleFeed";

    public static final String KEY_IS_RSS_FEED = "isRss";

    public static final String KEY_LINK_OF_ICON_FEED = "linkOfIconFeed";

    public static final String KEY_LINK = "url";

    public static final String KEY_TITLE = "title";

    public static final String KEY_DESCRIPTION = "description";

    public static final String KEY_DATE = "date";

    public SqlDB(final Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public final void onCreate(SQLiteDatabase sqLiteDatabase)
    {
        sqLiteDatabase.execSQL("create table " + TABLE_ALL_ITEMS + "(" + KEY_ID
                + " integer primary key," + KEY_FEED + " text," + KEY_TITLE + " text," + KEY_LINK + " text," + KEY_DESCRIPTION + " text,"
                + KEY_LINK_OF_ICON_FEED + " text," + KEY_DATE + " text" + ")");

        sqLiteDatabase.execSQL("create table " + TABLE_ALL_SAVED_ITEMS + "(" + KEY_ID
                + " integer primary key," + KEY_FEED + " text," + KEY_TITLE + " text," + KEY_LINK + " text," + KEY_DESCRIPTION + " text,"
                + KEY_DATE + " text" + ")");

        sqLiteDatabase.execSQL("create table " + TABLE_ALL_FEEDS
                + "(" + KEY_ID + " integer primary key," + KEY_LINK_FEED + " text," + KEY_LINK_OF_RSS_OR_ATOM_FEED + " text,"
                + KEY_TITLE_FEED + " text," + KEY_LINK_OF_ICON_FEED + " text," + KEY_IS_RSS_FEED + " integer" + ")");
    }

    @Override
    public final void onUpgrade(final SQLiteDatabase sqLiteDatabase,final int i,final int i1) {
        sqLiteDatabase.execSQL("drop table if exists " + TABLE_ALL_ITEMS);
    }

    public static ArrayList<Item> getAllItemsFromDatabase(final Context context) throws SQLException
    {
        ArrayList<Item> AllItems = new ArrayList<>();
        Item item;
        val db = new SqlDB(context);
        val database = db.getWritableDatabase();
        val cursor = database.query(SqlDB.TABLE_ALL_ITEMS, null, null, null, null, null, null);

        if (cursor.moveToFirst())
        {
            val idIndex = cursor.getColumnIndex(SqlDB.KEY_ID);
            val channelIndex = cursor.getColumnIndex(SqlDB.KEY_FEED);
            val titleIndex = cursor.getColumnIndex(SqlDB.KEY_TITLE);
            val urlIndex = cursor.getColumnIndex(SqlDB.KEY_LINK);
            val descriptionIndex = cursor.getColumnIndex(SqlDB.KEY_DESCRIPTION);
            val dateIndex = cursor.getColumnIndex(SqlDB.KEY_DATE);
            val linkOfIcon = cursor.getColumnIndex(SqlDB.KEY_LINK_OF_ICON_FEED);
            do {
                item = new Item();
                cursor.getInt(idIndex);
                item.setTitleOfFeed(cursor.getString(channelIndex));
                item.setTitle(cursor.getString(titleIndex));
                item.setUrlLink(cursor.getString(urlIndex));
                item.setDescription(cursor.getString(descriptionIndex));
                item.setLinkOfIconFeed(cursor.getString(linkOfIcon));
                try
                {
                    item.setPublishedDate(DateFormat.getDateInstance(DateFormat.LONG, new Locale("ru", "RU")).parse(cursor.getString(dateIndex)));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                AllItems.add(item);
            }
            while (cursor.moveToNext());
        }

        cursor.close();
        database.close();
        db.close();
        return AllItems;
    }

    public static ArrayList<Feed> getAllFeedsFromDatabase(final Context context) throws SQLException
    {
        ArrayList<Feed> AllFeeds = new ArrayList<>();
        Feed feed;
        val db = new SqlDB(context);
        val database = db.getWritableDatabase();
        val cursor = database.query(SqlDB.TABLE_ALL_FEEDS, null, null, null, null, null, null);
        if (cursor.moveToFirst())
        {
            val idIndex = cursor.getColumnIndex(SqlDB.KEY_ID);
            val urlIndex = cursor.getColumnIndex(SqlDB.KEY_LINK_FEED);
            val urlFeedIndex = cursor.getColumnIndex(SqlDB.KEY_LINK_OF_RSS_OR_ATOM_FEED);
            val titleIndex = cursor.getColumnIndex(SqlDB.KEY_TITLE_FEED);
            val isRssIndex = cursor.getColumnIndex(SqlDB.KEY_IS_RSS_FEED);
            val linkOfIcon = cursor.getColumnIndex(SqlDB.KEY_LINK_OF_ICON_FEED);
            do {
                feed = new Feed();
                cursor.getInt(idIndex);
                feed.setLink(cursor.getString(urlIndex));
                feed.setLinkOfRssOrAtomFeed(cursor.getString(urlFeedIndex));
                feed.setTitle(cursor.getString(titleIndex));
                feed.setLinkOfIconOFFeed(cursor.getString(linkOfIcon));
                if(1 == cursor.getInt(isRssIndex))
                    feed.setRssFeed(true);
                else
                    feed.setRssFeed(false);
                AllFeeds.add(feed);
            }
            while (cursor.moveToNext());
        }

        cursor.close();
        database.close();
        db.close();
        return AllFeeds;
    }

    public static ArrayList<Item> getAllItemsForSelectedFeed(final Context context,final String title)
    {
        ArrayList<Item> AllItems = new ArrayList<>();
        Item item;
        val db = new SqlDB(context);
        val database = db.getWritableDatabase();
        val cursor = database.query(SqlDB.TABLE_ALL_ITEMS, null, SqlDB.KEY_FEED + " = ?", new String[]{title}, null, null, null);

        if (cursor.moveToFirst())
        {
            val idIndex = cursor.getColumnIndex(SqlDB.KEY_ID);
            val channelIndex = cursor.getColumnIndex(SqlDB.KEY_FEED);
            val titleIndex = cursor.getColumnIndex(SqlDB.KEY_TITLE);
            val urlIndex = cursor.getColumnIndex(SqlDB.KEY_LINK);
            val descriptionIndex = cursor.getColumnIndex(SqlDB.KEY_DESCRIPTION);
            val dateIndex = cursor.getColumnIndex(SqlDB.KEY_DATE);
            val linkOfIcon = cursor.getColumnIndex(SqlDB.KEY_LINK_OF_ICON_FEED);
            do {
                item = new Item();
                cursor.getInt(idIndex);
                item.setTitleOfFeed(cursor.getString(channelIndex));
                item.setTitle(cursor.getString(titleIndex));
                item.setUrlLink(cursor.getString(urlIndex));
                item.setDescription(cursor.getString(descriptionIndex));
                item.setLinkOfIconFeed(cursor.getString(linkOfIcon));
                try
                {
                    item.setPublishedDate(DateFormat.getDateInstance(DateFormat.LONG, new Locale("ru", "RU")).parse(cursor.getString(dateIndex)));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if(title.equals(item.getTitleOfFeed()))
                    AllItems.add(item);
            }
            while (cursor.moveToNext());

        }

        cursor.close();
        database.close();
        db.close();
        return AllItems;
    }

    public static boolean suchAnItemAlreadyAdded(final SQLiteDatabase database,final String titleOfItem,final String linkOfItem)
    {
        boolean alreadyAdded = false;
        Cursor cursor = null;
        try {
            cursor = database.query(SqlDB.TABLE_ALL_ITEMS, new String[]{SqlDB.KEY_TITLE, SqlDB.KEY_LINK},
                    SqlDB.KEY_TITLE + " = ? or " + SqlDB.KEY_LINK + " = ?",
                    new String[]{titleOfItem, linkOfItem}, null, null, null);
            if (cursor.getCount() > 0)
                alreadyAdded = true;
        }
        finally
        {
            if(null != cursor)
                cursor.close();
        }

        return alreadyAdded;
    }

    public static void deleteSelectedFeeds(final Context context,final String titleFeed)
    {
        val db = new SqlDB(context);
        val database = db.getWritableDatabase();
        database.delete(SqlDB.TABLE_ALL_ITEMS,SqlDB.KEY_FEED + " = ?" , new String[]{titleFeed});
        database.delete(SqlDB.TABLE_ALL_FEEDS, SqlDB.KEY_TITLE_FEED + " = ?", new String[]{titleFeed});
        database.close();
        db.close();
    }
}
