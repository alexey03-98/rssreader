package com.nelyubin.rssreader.adapters;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.nelyubin.rssreader.R;
import com.nelyubin.rssreader.controller.NewsActivity;
import com.nelyubin.rssreader.model.Item;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public final class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ItemHolder>
{
    private List<Item> items;

    final private DisplayImageOptions displayOptions = new DisplayImageOptions.Builder()
            .cacheInMemory(true)
            .cacheOnDisk(true)
            .build();

    @Override
    public final ItemAdapter.ItemHolder onCreateViewHolder(final ViewGroup parent, final int viewType)
    {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        final View view = inflater.inflate(R.layout.data_view, parent, false);

        return new ItemHolder(view);
    }


    public void setItems(List<Item> list) {
        items = list;
        Collections.sort(items, Item.DateComparator);
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(final ItemHolder holder,final int position)
    {
        holder.titleOfFeedTextView.setText(items.get(holder.getAdapterPosition()).getTitleOfFeed());
        holder.titleOfItemTextView.setText(items.get(holder.getAdapterPosition()).getTitle());
        ImageLoader.getInstance().displayImage(items.get(holder.getAdapterPosition()).getLinkOfIconFeed(),holder.iconOfFeedImageView,displayOptions);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public final void onClick(View view)
            {
                Intent intent = NewsActivity.intentForStartNewsActivity(holder.itemView.getContext(),
                        items.get(holder.getAdapterPosition()).getDescription(),
                        items.get(holder.getAdapterPosition()).getUrlLink());
                holder.itemView.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public final int getItemCount() {
        return items.size();
    }


    class ItemHolder extends RecyclerView.ViewHolder
    {

        private TextView titleOfFeedTextView;

        private ImageView iconOfFeedImageView;

        private TextView titleOfItemTextView;

        private ItemHolder(final View itemLayoutView)
        {
            super(itemLayoutView);
            titleOfFeedTextView = itemLayoutView.findViewById(R.id.titleOfFeedDataView);
            iconOfFeedImageView = itemLayoutView.findViewById(R.id.iconOfFeedDataView);
            titleOfItemTextView = itemLayoutView.findViewById(R.id.titleOfItemDataView);
        }
    }

}
