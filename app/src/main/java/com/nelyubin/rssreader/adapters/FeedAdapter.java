package com.nelyubin.rssreader.adapters;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.nelyubin.rssreader.R;
import com.nelyubin.rssreader.controller.ItemsOfSelectedFeedActivity;
import com.nelyubin.rssreader.database.SqlDB;
import com.nelyubin.rssreader.model.Feed;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

public final class FeedAdapter extends RecyclerView.Adapter<FeedAdapter.FeedHolder>
{
    private ArrayList<Feed> feeds;

    final private DisplayImageOptions displayOptions = new DisplayImageOptions.Builder()
            .cacheInMemory(true)
            .cacheOnDisk(true)
            .build();

    public FeedAdapter(ArrayList<Feed> feeds)
    {
        this.feeds = feeds;
    }

    @Override
    public final FeedAdapter.FeedHolder onCreateViewHolder(final ViewGroup parent, final int viewType)
    {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View view = inflater.inflate(R.layout.feed_view, parent, false);
        return new FeedAdapter.FeedHolder(view);
    }

    @Override
    public final void onBindViewHolder(final FeedAdapter.FeedHolder holder,final int position)
    {
        holder.titleOfFeed.setText(feeds.get(holder.getAdapterPosition()).getTitle());
        ImageLoader.getInstance().displayImage(feeds.get(holder.getAdapterPosition()).getLinkOfIconOFFeed(), holder.iconOfFeed, displayOptions);
        /*holder.itemView.setOnLongClickListener(new View.OnLongClickListener()
        {
            @Override
            public final boolean onLongClick(View view)
            {
                SqlDB.deleteSelectedFeeds(view.getContext(), feeds.get(holder.getAdapterPosition()).getTitle());
                final Intent intent = new Intent(holder.itemView.getContext(), ListOfFeedActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                holder.itemView.getContext().startActivity(intent);
                return false;
            }
        });
*/
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent intent = ItemsOfSelectedFeedActivity.startItemsOfSelectedFeed(holder.itemView.getContext(),
                        feeds.get(holder.getAdapterPosition()).getTitle());
                holder.itemView.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public final int getItemCount() {
        return feeds.size();
    }

    class FeedHolder extends RecyclerView.ViewHolder
    {
        private TextView titleOfFeed;

        private ImageView iconOfFeed;

        private FeedHolder(final View itemLayoutView)
        {
            super(itemLayoutView);
            titleOfFeed = itemLayoutView.findViewById(R.id.titleOfFeedTextView);
            iconOfFeed = itemLayoutView.findViewById(R.id.iconOfFeed);
        }
    }
}


