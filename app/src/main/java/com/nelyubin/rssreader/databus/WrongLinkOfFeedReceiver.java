package com.nelyubin.rssreader.databus;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;
import com.nelyubin.rssreader.R;

public final class WrongLinkOfFeedReceiver extends BroadcastReceiver
{
    public static final String WRONG_LINK_OF_FEED = "WrongLinkOfFeed";
    @Override
    public void onReceive(Context context, Intent intent)
    {
        Toast.makeText(context, context.getString(R.string.checkTheCorrectnessOfInputData), Toast.LENGTH_SHORT).show();
    }
}
