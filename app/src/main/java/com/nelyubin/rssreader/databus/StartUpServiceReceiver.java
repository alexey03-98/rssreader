package com.nelyubin.rssreader.databus;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.nelyubin.rssreader.service.BackgroundDataRefreshService;

public class StartUpServiceReceiver extends BroadcastReceiver {

    @Override
    public final void onReceive(final Context context, final Intent intent)
    {
        final Intent iStartService = new Intent(context, BackgroundDataRefreshService.class);
        context.startService(iStartService);
    }
}
