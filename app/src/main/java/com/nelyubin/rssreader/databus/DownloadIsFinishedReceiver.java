package com.nelyubin.rssreader.databus;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.nelyubin.rssreader.controller.MainActivity;
import com.nelyubin.rssreader.service.BackgroundAddingChannel;

public final class DownloadIsFinishedReceiver extends BroadcastReceiver
{
    public static final String DOWNLOAD_IS_FINISHED = "DownloadIsFinished";
    @Override
    public void onReceive(Context context, Intent intent)
    {
        if(intent.getBooleanExtra(DOWNLOAD_IS_FINISHED,false))
        {
            context.stopService(new Intent(context, BackgroundAddingChannel.class));
            context.startActivity(new Intent(context, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
        }
    }
}
