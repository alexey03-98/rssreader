package com.nelyubin.rssreader.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.nelyubin.rssreader.database.SqlDB;
import com.nelyubin.rssreader.network.ThreadForAddingAndRefreshingFeeds;

public class BackgroundDataRefreshService extends Service
{
    private Thread thread;

    private SqlDB sqlDB;

    @Override
    public final void onCreate()
    {
        sqlDB = new SqlDB(this);
        thread = new Thread(new ThreadForAddingAndRefreshingFeeds(this,"",sqlDB,true));
        super.onCreate();
    }

    @Override
    public final int onStartCommand(final Intent intent,final int flags,final int startId)
    {
        if(!thread.isAlive())
        {
            thread.start();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    public Thread getThreadForAddingChannel()
    {
        return this.thread;
    }

    @Override
    public final void onDestroy()
    {
        sqlDB.close();
        thread.interrupt();
        super.onDestroy();
    }
    @Nullable
    @Override
    public final IBinder onBind(Intent intent) {
        return null;
    }
}
