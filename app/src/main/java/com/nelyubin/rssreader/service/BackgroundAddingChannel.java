package com.nelyubin.rssreader.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import com.nelyubin.rssreader.network.ThreadForAddingAndRefreshingFeeds;
import com.nelyubin.rssreader.database.SqlDB;

public class BackgroundAddingChannel extends Service
{
    private SqlDB db;

    private Thread thread;

    private static final String KEY_LINK_FOR_START_SERVICE = "LinkOfFeed";

    private static final String KEY_IS_REFRESH_FEEDS = "isRefresh";
    @Override
    public final void onCreate()
    {

        db = new SqlDB(this);

        super.onCreate();
    }

    @Override
    public final int onStartCommand(final Intent intent, final int flags, final int startId)
    {
        final String linkOfFeed = (String) intent.getExtras().get(KEY_LINK_FOR_START_SERVICE);
        final boolean isRefreshFeeds = (boolean) intent.getBooleanExtra(KEY_IS_REFRESH_FEEDS, false);
        thread = new  Thread(new ThreadForAddingAndRefreshingFeeds(this,linkOfFeed,db,isRefreshFeeds));
        thread.start();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public final void onDestroy()
    {
        db.close();
        thread.interrupt();
        super.onDestroy();
    }

    public static Intent startServiceForAddingChannel(Context context, String linkOfFeed, boolean isRefreshFeeds)
    {
        Intent intent = new Intent(context,BackgroundAddingChannel.class);
        intent.putExtra(KEY_LINK_FOR_START_SERVICE,linkOfFeed);
        intent.putExtra(KEY_IS_REFRESH_FEEDS,isRefreshFeeds);
        return intent;
    }


    @Nullable
    @Override
    public final IBinder onBind(Intent intent) {
        return null;
    }
}
