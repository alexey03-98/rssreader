package com.nelyubin.rssreader.controller;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.nelyubin.rssreader.R;

public final class NewsActivity extends Activity
{
    public static final String KEY_CONTENT_OF_ITEM = "contentOfItem";

    public static final String KEY_LINK_TO_FULL_CONTENT = "linkToFullContent";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        final String linkToFullNews = (String)getIntent().getExtras().get(KEY_LINK_TO_FULL_CONTENT);
        TextView contentOfItem = findViewById(R.id.contentOfItemTextView);
        contentOfItem.setText(Html.fromHtml((String)this.getIntent().getExtras().get(KEY_CONTENT_OF_ITEM)));
        TextView readFullNews = findViewById(R.id.readFullNews);
        readFullNews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                startActivity(FullNewsActivity.intentForStartActivityWithFullNews(view.getContext(),linkToFullNews));
            }
        });
    }

    public static Intent intentForStartNewsActivity(Context context, String contentOfItem, String linkForFullContent)
    {
        Intent intent = new Intent(context, NewsActivity.class);
        intent.putExtra(NewsActivity.KEY_CONTENT_OF_ITEM,contentOfItem);
        intent.putExtra(NewsActivity.KEY_LINK_TO_FULL_CONTENT, linkForFullContent);
        return intent;
    }
}
