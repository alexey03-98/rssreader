package com.nelyubin.rssreader.controller;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.nelyubin.rssreader.R;

public final class FullNewsActivity extends Activity {

    public static final String KEY_LINK_TO_FULL_NEWS = "linkToFullNews";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_news);
        WebView webView = findViewById(R.id.fullNewsWebView);
        webView.loadUrl((String)this.getIntent().getExtras().get(KEY_LINK_TO_FULL_NEWS));
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient());
    }

    public static Intent intentForStartActivityWithFullNews(Context context, String linkToFullNews)
    {
        Intent intent = new Intent(context, FullNewsActivity.class);
        intent.putExtra(KEY_LINK_TO_FULL_NEWS,linkToFullNews);
        return intent;
    }
}
