package com.nelyubin.rssreader.controller;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;

import com.nelyubin.rssreader.R;
import com.nelyubin.rssreader.adapters.ItemAdapter;
import com.nelyubin.rssreader.database.SqlDB;

public class ItemsOfSelectedFeedActivity extends AppCompatActivity
{
    public static final String KEY_TITLE_OF_SELECTED_FEED = "titleOfSelectedFeed";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_items_of_selected_feed);
        RecyclerView recyclerView = findViewById(R.id.listOfItemsSelectedFeed);
        ItemAdapter itemAdapter = new ItemAdapter();
        recyclerView.setAdapter(itemAdapter);
        itemAdapter.setItems(SqlDB.getAllItemsForSelectedFeed(this,(String)getIntent().getExtras().get(KEY_TITLE_OF_SELECTED_FEED)));
    }

    public static Intent startItemsOfSelectedFeed(Context context, String titleOfSelectedFeed)
    {
        Intent intent = new Intent(context, ItemsOfSelectedFeedActivity.class);
        intent.putExtra(KEY_TITLE_OF_SELECTED_FEED, titleOfSelectedFeed);
        return intent;
    }
}
