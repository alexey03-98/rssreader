package com.nelyubin.rssreader.controller;

import android.app.Activity;
import android.content.IntentFilter;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import com.nelyubin.rssreader.R;
import com.nelyubin.rssreader.databus.DownloadIsFinishedReceiver;
import com.nelyubin.rssreader.databus.WrongLinkOfFeedReceiver;
import com.nelyubin.rssreader.listeners.AddChannelAndStartDownloadButtonListener;

public final class AddChannelActivity extends Activity
{
    private DownloadIsFinishedReceiver downloadIsFinishedReceiver;

    private WrongLinkOfFeedReceiver wrongLinkOfFeedReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_channel);
        downloadIsFinishedReceiver = new DownloadIsFinishedReceiver();
        wrongLinkOfFeedReceiver = new WrongLinkOfFeedReceiver();
        EditText linkOfFeed = findViewById(R.id.linkOfFeedEditText);
        Button addChannelButton = findViewById(R.id.addChannelButton);
        addChannelButton.setOnClickListener(new AddChannelAndStartDownloadButtonListener(linkOfFeed));
    }

    @Override
    protected void onResume()
    {
        final IntentFilter intentFilterDownload = new IntentFilter(DownloadIsFinishedReceiver.DOWNLOAD_IS_FINISHED);
        final IntentFilter intentFilterLink = new IntentFilter(WrongLinkOfFeedReceiver.WRONG_LINK_OF_FEED);
        registerReceiver(downloadIsFinishedReceiver,intentFilterDownload);
        registerReceiver(wrongLinkOfFeedReceiver,intentFilterLink);
        super.onResume();
    }

    @Override
    protected void onPause()
    {
        unregisterReceiver(downloadIsFinishedReceiver);
        unregisterReceiver(wrongLinkOfFeedReceiver);
        super.onPause();
    }
}
