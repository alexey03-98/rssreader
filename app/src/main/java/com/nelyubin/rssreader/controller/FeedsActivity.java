package com.nelyubin.rssreader.controller;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;

import com.nelyubin.rssreader.R;
import com.nelyubin.rssreader.adapters.FeedAdapter;
import com.nelyubin.rssreader.database.SqlDB;

public class FeedsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_of_feeds);
        RecyclerView recyclerView = findViewById(R.id.listOfFeedsActivity);
        recyclerView.setAdapter(new FeedAdapter(SqlDB.getAllFeedsFromDatabase(this)));
    }
}
